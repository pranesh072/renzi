package pranesh.renzi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.Firebase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by pranesh on 05-May-16.
 */
public class post extends AppCompatActivity {
    GPSTracker gps;

private Firebase mFirebase;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapter1;

    ArrayList<String> itemList;
    ArrayList<String> itemList1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.post);
        mFirebase = new Firebase("https://burning-inferno-7421.firebaseio.com");
       final EditText address = (EditText) this.findViewById(R.id.address);
        final EditText rent =(EditText)this.findViewById(R.id.rent);
        Button sendButton = (Button) this.findViewById(R.id.submit);
        Button sendButton1 = (Button) this.findViewById(R.id.location_button);
        final EditText longi=(EditText)this.findViewById(R.id.longi) ;
        final EditText lati=(EditText)this.findViewById(R.id.lati) ;



        sendButton1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // create class object
                gps = new GPSTracker(post.this);

                // check if GPS enabled
                if(gps.canGetLocation()){
                    double latitude=0.0;
                    double longitude=0.0;

                  latitude  = gps.getLatitude();
                  longitude= gps.getLongitude();
                    String stringdouble= Double.toString(latitude);
                    String stringdouble1= Double.toString(latitude);

                  longi.setText(stringdouble);
                  lati.setText(stringdouble1);
//

                    // \n is for new line
                    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                }else{
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }
        });
        String[] items={""};
        String[] fact_item={""};
        Button btAdd1=(Button)this.findViewById(R.id.btAdd2);
Button btAdd = (Button)this.findViewById(R.id.btAdd);
       final EditText rule=(EditText)this.findViewById(R.id.rules) ;
        final EditText fact=(EditText)this.findViewById(R.id.facilities) ;

        itemList=new ArrayList<String>(Arrays.asList(items));
        itemList1=new ArrayList<String>(Arrays.asList(fact_item));

        adapter=new ArrayAdapter<String>(this,R.layout.list_item,R.id.txtview,itemList);
        ListView listV=(ListView)findViewById(R.id.list);
        listV.setAdapter(adapter);
        adapter1=new ArrayAdapter<String>(this,R.layout.list_item,R.id.txtview,itemList);
        ListView list1=(ListView)findViewById(R.id.list);
        listV.setAdapter(adapter);



        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              final  String newItem=rule.getText().toString();
                // add new item to arraylist
                itemList.add(newItem);
                // notify listview of data changed
                adapter.notifyDataSetChanged();
            }

        });
        btAdd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final  String newItem=fact.getText().toString();
                // add new item to arraylist
                itemList1.add(newItem);
                // notify listview of data changed
                adapter.notifyDataSetChanged();
            }

        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rento = rent.getText().toString();
                String text = address.getText().toString();
                Map<String,Object> values = new HashMap<>();
                values.put("name", rento);
                values.put("text", text);
                values.put("rules",itemList);
                values.put("facilities",itemList1);
                mFirebase.push().setValue(values);
                address.setText("");
            }
        });


    }
}

